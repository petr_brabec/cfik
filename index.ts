import { Container as C } from "./modules/Container";
import { Layout as L } from "./modules/Layout/Layout";
import { NavigationPosition as N } from "./modules/Layout/NavigationPosition";
import { Page as P } from "./modules/Page";
import { Modal as M } from "./modules/Modal/Modal";

export const Container = C;
export const Page = P;
export const Modal = M;
export const Layout = L;
export const NavigationPosition = N;